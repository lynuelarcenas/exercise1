using System;

public struct Student {
	
    public string id { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string age { get; set; }
    
};


public class TestEnrollmentSystem {
	
	public static void Main() {
		Student Stud = new Student();
		Student[] StudList = new Student[50];
		int studCount = 0;
		bool loopMainMenu = true;
		
		do {
				
			Console.WriteLine("\nMAIN MENU\n[1] Enroll\n[2] Student Information\n[3] Exit");
			Console.Write("Enter choice: ");
			int mainMenuChoice = Convert.ToInt16(Console.ReadLine());
			
			
			if (mainMenuChoice == 1){
				
				bool loopEnroll = true;
				do {
					Console.WriteLine("\nENROLLMENT");
					Console.Write("Enter student ID: ");
					Stud.id = Console.ReadLine();
					Console.Write("Enter First Name: ");
					Stud.firstName = Console.ReadLine();
					Console.Write("Enter Last Name: ");
					Stud.lastName = Console.ReadLine();
					Console.Write("Enter Age: ");
					Stud.age = Console.ReadLine();
					
					StudList[studCount] = Stud;
					studCount++;
					
					Console.WriteLine("\nStudent added.");
					Console.Write("Add another student (y/n)? ");
					string addStdntChoice = Console.ReadLine();
					if (addStdntChoice == "y"){loopEnroll = true;}
					else if (addStdntChoice == "n"){loopEnroll = false;}
				
				} while (loopEnroll);
				
				
			
			}
			
			else if (mainMenuChoice == 2){
				Console.WriteLine("\nSTUDENT INFORMATION");
				Console.Write("Enter ID Number: ");
				string searchId = Console.ReadLine();
				
				for (int searchCount = 0 ; searchCount < studCount ; searchCount++){
					if (StudList[searchCount].id == searchId){
						Console.WriteLine("\nID number: "+StudList[searchCount].id);
						Console.WriteLine("First name: "+StudList[searchCount].firstName);
						Console.WriteLine("Last name: "+StudList[searchCount].lastName);
						Console.WriteLine("Age: "+StudList[searchCount].age);
					}
					else {Console.Write("\nID does not exist!");}
				}
				
			}
			else if (mainMenuChoice == 3){
				Console.WriteLine("\nExiting...");
				loopMainMenu = false;
			}
		} while (loopMainMenu);
	}
}